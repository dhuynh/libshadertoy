# libshadertoy - 15-uniforms

This example shows how to use custom uniforms defined at runtime
with a custom shader, using libshadertoy.

## Dependencies

* libglfw3-dev
* cmake
* git
* g++
* ca-certificates
* pkg-config

## Copyright

libshadertoy - Vincent Tavernier <vincent.tavernier@inria.fr>

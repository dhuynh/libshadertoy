#include <epoxy/gl.h>

#include <GLFW/glfw3.h>

#include <shadertoy/Shadertoy.hpp>

#include "uniforms.hpp"

#define IMPLEMENT_UNIFORM_STATE
#include "uniforms.hpp"

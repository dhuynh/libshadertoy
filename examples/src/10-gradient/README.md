# libshadertoy - 10-gradient

This example shows how the *gradient* default shadertoy can be rendered using
libshadertoy.

## Dependencies

* libglfw3-dev
* cmake
* git
* g++
* ca-certificates
* pkg-config

## Copyright

libshadertoy - Vincent Tavernier <vincent.tavernier@inria.fr>

#include "shadertoy/uniform_state_decl.hpp"

DECLARE_DYNAMIC_UNIFORM(iDynamicFloats, float, glm::vec2, glm::vec3, glm::vec4);

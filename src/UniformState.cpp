#include "stdafx.hpp"
#include "shadertoy/ShadertoyError.hpp"
#include "shadertoy/OpenGL/OpenGL.hpp"

#include "shadertoy/UniformState.hpp"

#define IMPLEMENT_UNIFORM_STATE
#include "shadertoy/UniformState_impl.hpp"

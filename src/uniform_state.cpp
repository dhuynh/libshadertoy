#include <epoxy/gl.h>

#include "shadertoy/gl.hpp"

#include "shadertoy/uniform_state.hpp"

#define IMPLEMENT_UNIFORM_STATE
#include "shadertoy/uniform_state_impl.hpp"
